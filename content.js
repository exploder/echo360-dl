/* jshint moz: true, multistr: true, esversion: 6 */

var url = window.location.href;
var to = url.lastIndexOf('/');
to = to == -1 ? url.length : to + 1;
url = url.substring(0, to);

var all = [];

fetch(url + "syllabus")
.then(function(response) {
    return response.json();
})
.then(function(jiisonni) {
    var data = jiisonni.data;
    var ret = {};
    ret.command = "total";
    ret.total = 0;
    for(var i in data) {
        if(data[i].type === undefined) {
            console.log("Type is undefined!");
        } else {
            var dataType = data[i].type;
            if(dataType === "SyllabusGroupType") {
                les = data[i].lessons;
                for(i in les) {
                        ret.total++;
                }
            } else if(dataType === "SyllabusLessonType") {
                    ret.total++;
            }
        }
    }
    browser.runtime.sendMessage(ret);

    promises = [];
    for(i in data) {
        if(data[i].type === undefined) {
            console.log("Type is undefined!");
        } else {
            let dataType = data[i].type;
            if(dataType === "SyllabusGroupType") {
                var msg = {};
                msg.command = "startgroup";
                msg.name = data[i].groupInfo.name;
                promises.push(msg);
                for (let les of data[i].lessons) {
                    promises.push(lesson(les));
                }
                var msg2 = {};
                msg2.command = "endgroup";
                promises.push(msg2);
            } else if(dataType === "SyllabusLessonType") {
                promises.push(lesson(data[i]));
            }
        }
    }
    Promise.all(promises).then(values => {
        for (let value of values) {
            if (value === null) {
                continue;
            }
            browser.runtime.sendMessage(value);
        }
        ret = {};
        ret.command = "loaded";
        browser.runtime.sendMessage(ret);
    });
});

function lesson(lessondata) {
    let lessonId = lessondata.lesson.lesson.id;
    return fetch(`https://echo360.org.uk/lesson/${lessonId}/classroom`)
    .then(response => {
        return response.text();
    }).then(text => {
        const jsonStrings = text.match(/Echo\["classroomApp"\]\("(.*)"\);/);
        const jsonString = jsonStrings[1].replace(/\\\"/g, "\"");
        const echoData = JSON.parse(jsonString);

        let lesson = {};
        lesson.name = echoData.lesson.name;
        lesson.videos = [];
        let buildLinkManually = false;
        let dateStr = "";
        if (echoData.lesson.timing !== undefined) {
            dateStr = echoData.lesson.timing.start;
        } else {
            dateStr = echoData.lesson.createdAt;
        }

        if (echoData.video === null) {
            return null;
        }
        
        for (let media of echoData.video.playableMedias) {
            if (media.isHls && !buildLinkManually) {
                buildLinkManually = true;
            }
            
            if (buildLinkManually) {
                const type = media.trackType;
                let lessonMedia = {};
                if (type.includes("Video") && type.length == 1) {
                    // Ignore
                    continue;
                } else if (type.includes("Audio") && type.length == 1) {
                    // Audio track
                    lessonMedia.type = "audio";
                    lessonMedia.id = echoData.video.mediaId;
                    lessonMedia.dateStr = dateStr;
                    lessonMedia.url = "https://content.echo360.org.uk/0000." +
                                    echoData.lesson.institutionId + "/" +
                                    echoData.video.mediaId + "/1/audio.mp3";
                } else if (type.length == 2) {
                    // Video track
                    lessonMedia.type = media.sourceIndex == 1 ? "primary" : "secondary";
                    lessonMedia.id = echoData.video.mediaId;
                    lessonMedia.dateStr = dateStr;
                    if (media.quality.includes(1)) {
                        lessonMedia.hd = {
                            url: "https://content.echo360.org.uk/0000." +
                                echoData.lesson.institutionId + "/" +
                                echoData.video.mediaId + "/1/hd" +
                                media.sourceIndex + ".mp4"
                        };
                    }
                    if (media.quality.includes(0)) {
                        lessonMedia.sd = {
                            url: "https://content.echo360.org.uk/0000." +
                                echoData.lesson.institutionId + "/" +
                                echoData.video.mediaId + "/1/sd" +
                                media.sourceIndex + ".mp4"
                        };
                    }
                } else {
                    // Ignore
                    continue;
                }

                lesson.videos.push(lessonMedia);
                continue;
            } else {
                // We can directly grab the link
                let lessonMedia = {};
                const type = media.trackType;
                if (type.includes("Audio") && type.length == 1) {
                    lessonMedia.type = "audio";
                    lessonMedia.id = echoData.video.mediaId;
                    lessonMedia.dateStr = dateStr;
                    lessonMedia.url = media.uri;
                    lesson.videos.push(lessonMedia);
                } else if (type.length == 2) {
                    let retType = media.sourceIndex == 1 ? "primary" : "secondary";
                    let alreadyFound = false;
                    for (let index in lesson.videos) {
                        if (lesson.videos[index].type === retType) {
                            if (media.quality.includes(1)) {
                                lesson.videos[index].hd = {
                                    url: media.uri
                                };
                            } else if (media.quality.includes(0)) {
                                lesson.videos[index].sd = {
                                    url: media.uri
                                };
                            }
                            alreadyFound = true;
                            break;
                        }
                    }
                    if (alreadyFound) {
                        continue;
                    }
                    lessonMedia.type = retType;
                    lessonMedia.id = echoData.video.mediaId;
                    lessonMedia.dateStr = dateStr;
                    if (media.quality.includes(1)) {
                        lessonMedia.hd = {
                            url: media.uri
                        };
                    } else if (media.quality.includes(0)) {
                        lessonMedia.sd = {
                            url: media.uri
                        };
                    }
                    lesson.videos.push(lessonMedia);
                }
            }
        }
        lesson.command = "addlesson";
        return lesson;
    });
}