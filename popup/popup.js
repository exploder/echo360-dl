/* jshint moz: true, multistr: true */

browser.tabs.executeScript({
    file: "../content.js"
});

var currentElement = document.getElementById("videos");
var insidePanel = false;
var id = 0;
var lessons = [];
var allVideos = [];
var selectedVideos = [];
var selectedQualities = [];
var lessonId = 0;
var videoId = 0;
var progress = 0;
var total = 0;

function message(msg) {
    //console.log("Popup received JSON data from content script:");
    //console.log(JSON.stringify(msg));
    if(msg.command === "addlesson") {
        lessons.push(msg);
        progress++;
        document.getElementById("thebar").style.width = (progress/total)*100 + "%";
        for(var i in msg.videos) {
            msg.videos[i].lesson = lessonId;
            allVideos.push(msg.videos[i]);
            selectedVideos.push(false);
            var row = document.createElement("tr");
            row.id = videoId;

            row.onclick = function() {
                this.classList.toggle("selected");
                if(this.classList.contains("selected")) {
                    let id = this.id;
                    selectedVideos[id] = true;
                    //console.log(lessons[allVideos[id].lesson]);
                    updateSize();
                } else {
                    let id = this.id;
                    selectedVideos[id] = false;
                    updateSize();
                }
            };
            row.insertAdjacentHTML('afterbegin', '<td class="align-middle">' +
                                   escapeHTML(msg.name) + '</td>' +
                                   '<td class="align-middle">' +
                                   escapeHTML(msg.videos[i].type) + '</td>');

            td = document.createElement("td");
            td.className = "align-middle";

            if(msg.videos[i].type === "audio") {
                selectedQualities.push("audio");
                td.insertAdjacentHTML('afterbegin',"audio");
            } else {
                selectedQualities.push("hd");
                var sel = document.createElement("select");
                sel.className = "custom-select";
                sel.onclick = function(e) {
                    e.stopPropagation();
                };
                if(msg.videos[i].hd !== undefined) {
                    sel.insertAdjacentHTML('beforeend',
                    '<option value="hd">HD</option>');
                }
                if(msg.videos[i].sd !== undefined) {
                    sel.insertAdjacentHTML('beforeend',
                    '<option value="sd">SD</option>');
                }

                sel.onchange = function() {
                    selectedQualities[this.parentNode.parentNode.id] = this.value;
                    updateSize();
                };
                td.appendChild(sel);
            }
            row.appendChild(td);

            currentElement.appendChild(row);
            videoId++;
        }
        lessonId++;

    } else if(msg.command === "startgroup") {
        if(currentElement.innerHTML.trim() === "") {
            currentElement.parentNode.remove();
        }
        var elem = document.createElement("div");
        elem.className = "mt-3 mb-3";
        elem.innerHTML =
        '<h3>' + msg.name + '</h3>\
        <table class="table">\
            <thead>\
                <tr>\
                    <th>Name</th>\
                    <th>Type</th>\
                    <th>Quality</th>\
                </th>\
            </thead>\
            <tbody id="videos' + id + '">\
            </tbody>\
        </table>';
        document.getElementById("content").appendChild(elem);
        currentElement = document.getElementById("videos" + id);
        id++;
    } else if(msg.command === "endgroup") {
        var tab = document.createElement("table");
        tab.className = "table";
        tabHtml = 
        '<thead>' +
            '<tr>' +
                '<th>Name</th>' +
                '<th>Type</th>' +
                '<th>Quality</th>' +
            '</th>' +
        '</thead>' +
        '<tbody id="videos' + id + '">' +
        '</tbody>';
        tab.innerHTML = tabHtml;
        
        document.getElementById("content").appendChild(tab);
        currentElement = document.getElementById("videos" + id);
        id++;
    } else if(msg.command === "loaded") {
        setTimeout(function() {
            var theBar = document.getElementById("thebar");
            theBar.classList.toggle("progress-bar-striped");
            theBar.classList.toggle("progress-bar-animated");
            theBar.classList.toggle("bg-success", true);
            setTimeout(function() {
                document.getElementById("content").removeAttribute("hidden");
                document.getElementById("progressbar").setAttribute("hidden","hidden");
            }, 0);
        }, 0);
    } else if(msg.command === "total") {
        document.getElementById("progressbar").innerHTML = '<h4>Loading videos...</h4>\
        <div class="progress">\
            <div id="thebar" class="progress-bar progress-bar-striped progress-bar-animated" style="width: 0%"></div>\
        </div>';
        total = msg.total;
    }
}

function updateSize() {
    var num_sel = selectedVideos.filter(Boolean).length;

    var dl_btn = document.getElementById("download");
    if(num_sel > 0) {
        dl_btn.classList.toggle("disabled", false);
    } else {
        dl_btn.classList.toggle("disabled", true);
    }
}

const ESC_MAP = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;'
};

function escapeHTML(s, forAttribute) {
    return s.replace(forAttribute ? /[&<>'"]/g : /[&<>]/g, function(c) {
        return ESC_MAP[c];
    });
}

const tiedostokoot=["B","kB","MB","GB","TB"];
const vali=" ";
const desimaalit=2;//max desimaalit

function formatSize(unformatted) {
    var loppu = "";
    var uusi = 0;
    for(var i = 0; i < tiedostokoot.length; i++){
        if (unformatted.toString().length <= (i+1)*3){
            loppu = tiedostokoot[i];
            uusi = unformatted/(Math.pow(10,(i)*3 - desimaalit));
            uusi = Math.round(uusi);
            //console.log(uusi/Math.pow(10,desimaalit) + vali + loppu);
            break;
        }
    }
    return uusi/Math.pow(10,desimaalit) + vali + loppu;
}

browser.runtime.onMessage.addListener(message);

document.getElementById("selection").onchange = function() {
    if(this.value === "all") {
        for(let i in allVideos) {
            selectedVideos[i] = true;
            document.getElementById(i).classList.toggle("selected", true);
        }
    } else if(this.value === "none") {
        for(let i in allVideos) {
            selectedVideos[i] = false;
            document.getElementById(i).classList.toggle("selected", false);
        }
    } else if(this.value === "invert") {
        for(let i in allVideos) {
            selectedVideos[i] = !selectedVideos[i];
            document.getElementById(i).classList.toggle("selected");
        }
    } else if(this.value.startsWith("all_video")) {
        for(let i in allVideos) {
            if(allVideos[i].type !== "audio") {
                if(this.value.endsWith("hd") && allVideos[i].hd !== undefined) {
                    selectedVideos[i] = true;
                    selectedQualities[i] = "hd";
                    document.getElementById(i).getElementsByTagName("select")[0].value = "hd";
                    document.getElementById(i).classList.toggle("selected", true);
                } else if(this.value.endsWith("sd") && allVideos[i].hd !== undefined) {
                    selectedVideos[i] = true;
                    selectedQualities[i] = "sd";
                    document.getElementById(i).classList.toggle("selected", true);
                    document.getElementById(i).getElementsByTagName("select")[0].value = "sd";
                } else {
                    selectedVideos[i] = true;
                    document.getElementById(i).classList.toggle("selected", true);
                }
            }
        }
    } else if(this.value === "all_audio") {
        for(let i in allVideos) {
            if(allVideos[i].type === "audio") {
                selectedVideos[i] = true;
                document.getElementById(i).classList.toggle("selected", true);
            }
        }
    }
    updateSize();
    this.value = "s";
};

document.getElementById("download").onclick = function() {
    var num_sel = selectedVideos.filter(Boolean).length;
    urls = [];
    names = [];
    for(var i in selectedVideos) {
        if(selectedVideos[i]) {
            var url = "";
            
            var videoDate = new Date(allVideos[i].dateStr);
            var dateString = 
            videoDate.getUTCFullYear().toString().padStart(4, "0") +
            (videoDate.getUTCMonth() + 1).toString().padStart(2, "0") +
            videoDate.getUTCDate().toString().padStart(2, "0");

            if(selectedQualities[i] === "hd") {
                url = allVideos[i].hd.url;
            } else if(selectedQualities[i] === "sd") {
                url = allVideos[i].sd.url;
            } else if(selectedQualities[i] === "audio") {
                url = allVideos[i].url;
            }

            urls.push(url);
            names.push(
                dateString + '-' +
                sanitizeFilename(lessons[allVideos[i].lesson].name, "_")
                .replace(/ /g, "_").replace(/_{2,}/g, "_") + "-" +
                allVideos[i].type + "-" + selectedQualities[i] + 
                (selectedQualities[i] === "audio" ? ".mp3" : ".mp4")
            );
            
        }
    }
    var ret = {};
    ret.command = "download";
    ret.urls = urls;
    ret.names = names;
    browser.runtime.sendMessage(ret);
};

// from https://github.com/parshap/node-sanitize-filename/blob/a55b3ba097cafd79717e2b87735ab4885355aac4/index.js
const illegalRe = /[\/\?<>\\:\*\|":]/g;
const controlRe = /[\x00-\x1f\x80-\x9f]/g;
const reservedRe = /^\.+$/;
const windowsReservedRe = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i;
const windowsTrailingRe = /[\. ]+$/;

function sanitizeFilename(input, replacement) {
  var sanitized = input
    .replace(illegalRe, replacement)
    .replace(controlRe, replacement)
    .replace(reservedRe, replacement)
    .replace(windowsReservedRe, replacement)
    .replace(windowsTrailingRe, replacement);
  return sanitized;
}
