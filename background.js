/* jshint moz: true, multistr: true, esversion: 6 */

function click() {
    browser.tabs.executeScript({
        file: "content.js"
    });
}

function message(msg) {
    if (msg.command === "download") {
        downloadSequentially(msg.urls, msg.names, value => {});
    }
}

function downloadSequentially(urls, names, callback) {
    let index = 0;
    let currentId;

    browser.downloads.onChanged.addListener(onChanged);

    next();

    function next() {
        let txt = (urls.length - index - 1).toString();
        browser.browserAction.setBadgeText({
            text: txt
        });
        if (index >= urls.length) {
            browser.downloads.onChanged.removeListener(onChanged);
            browser.browserAction.setBadgeText({text: ""});
            callback();
            return;
        }
        const url = urls[index];
        
        if (url) {
            browser.downloads.download({
                url: url,
                filename: names[index],
                conflictAction: "uniquify"
            }, id => {
                currentId = id;
            });
        }
        index++;
    }

    function onChanged({
        id,
        state
    }) {
        if (id === currentId && state && state.current !== 'in_progress') {
            next();
        }
    }
}

browser.browserAction.onClicked.addListener(click);
browser.runtime.onMessage.addListener(message);
browser.browserAction.setBadgeBackgroundColor({
    color: "darkgray"
});